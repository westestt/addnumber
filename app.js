const express = require('express');
const path = require('path');
var addTwoNumbers = require('./addTwoNumbers');
const bodyParser =require('body-parser');

// Init app
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

/*
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
    //res.send("test")
});
*/
app.post('/calculate', function(req, res) {
    var x = parseInt(req.body.num1,10)
    var y = parseInt(req.body.num2,10)
    if (isNaN(x) || isNaN(y) ) {
        res.status(400)
    }
    var result = addTwoNumbers(x ,y )
    res.json(result);  

});

const port = 8080;
app.listen(port, () => console.log(`Server started on port ${port}`)); 

module.exports = app;